<!DOCTYPE html>
<html lang="fr">
<?php include_once ("head.php")?>
<body>
<?php include_once ("nav.php")?>
    <div class="all">
        <div class="social">                     
            <ul class="social">           
                <li><a href="https://www.facebook.com/mandzou" target="_blank" rel="noopener noreferrer"><img src="content/logo/facebook.png" alt="Lien Facebook" class="facebook" title="Follow on Facebook"></a>
                </li>
                <li><a href="https://www.instagram.com/mandzou/" target="_blank" rel="noopener noreferrer"><img src="content/logo/instagram.png" alt="Lien Instagram" class="instagram" title="Follow on Instagram"></a> 
                </li>
                <li><a href="http://www.etsy.com/fr/shop/Mandzou" target="_blank" rel="noopener noreferrer"><img src="content/logo/etsy.png" alt="Lien Etsy" class="etsy" title="Follow on Etsy"></a>
                </li>
            </ul>
        </div>
        <div>
            <div class="acceuil">
                <a href="index.php"><img src="content/logo/fleche-fine-contour-vers-la-gauche.png" alt=""><h2>accueil</h2></a>
            </div>
            <div class="wedding">   
                <div class="wedding--banner">
                    <span><p>Chère "Mademoiselle", sur le point de devenir " Madame "</p></span>
                </div>
                <div class="wedding--details">
                    <div>  
                        <div class="wedding--photo">              
                            <img src="content/Photo/Mariage 4.jpg" alt="">
                            <img src="content/Photo/Mariage 5.jpg" alt="">                        
                            <img src="content/Photo/Mariage 6.jpg" alt="">
                        </div>
                        <div class="wedding--text">
                            <p>
                                Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                                Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                                Vient le détail de l’accessoire de coiffure <br>
                                « que mettre ? » <br> 
                                « Comment se coiffer ? »<br>
                                Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                            </p>
                            <p>
                                A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                                Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                                Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                            </p>
                        </div>
                    </div>
                    <img src="content/Photo/Mariage.jpg" alt="">
                </div>
            </div> 
            <?php include_once ("CTA.php")?>
        </div>
        <span id="retour"><p>Haut de la page</p></span>
        <?php include_once ("footer.php")?>
    </div>
</body>
<script src="jquery-3.6.0.min.js"></script>
<script src="script.js"></script>
</html>