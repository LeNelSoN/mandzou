<div class="CTA">
    <h2>Suivez-Moi Sur Les Réseaux Sociaux</h2>
    <div class="CTA--container">
        <a href="https://www.facebook.com/mandzou" target="_blank" rel="noopener noreferrer"><img src="content/logo/facebook2.png" alt="Lien Facebook" class="facebook" title="Follow on Facebook"></a>
        <a href="https://www.instagram.com/mandzou/" target="_blank" rel="noopener noreferrer"><img src="content/logo/instagram2.png" alt="Lien Instagram" class="instagram" title="Follow on Instagram"></a> 
        <a href="http://www.etsy.com/fr/shop/Mandzou" target="_blank" rel="noopener noreferrer"><img src="content/logo/etsy2.png" alt="Lien Etsy" class="etsy" title="Follow on Etsy"></a>
    </div>
</div>