<!DOCTYPE html>
<html lang="fr">
<?php include_once ("head.php")?>
<body>
<?php include_once ("nav.php")?>
<div class="all cabaret"> 
    <div class="acceuil">
        <a href="index.php"><img src="content/logo/fleche-fine-contour-vers-la-gauche.png" alt=""><h2>accueil</h2></a>
    </div>   
    <div class="wedding">   
        <div class="article--banner dolls__bckgrnd--1">
            <span><p>Dolls House</p></span>
        </div>
        <div class="article--details article__details--alt dolls__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content\Cabaret\2021 02 Dolls House (1).jpg" alt="">
                    <img src="content\Cabaret\2021 02 Dolls House (3).jpg" alt="">                        
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content\Cabaret\2021 02 Dolls House (2).jpg" alt="">
        </div>
    </div> 
    <?php include_once ("CTA.php")?>
    <div class="wedding">   
        <div class="article--banner  master__bckgrnd--1">
            <span><p>La Voix du Maitre</p></span>
        </div>
        <div class="article--details article__details--alt master__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content\Cabaret\2021 03 La Voix du Maitre (62).jpg" alt="">
                    <img src="content\Cabaret\2021 03 La Voix du Maitre (90).jpg" alt="">                        
                    <img src="content\Cabaret\2021 03 La Voix du Maitre (74).jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content\Cabaret\2021 03 La Voix du Maitre (68).jpg" alt="">
        </div>
    </div>  
    <div class="wedding">   
        <div class="article--banner phoenix__bckgrnd--1">
            <span><p>Phoenix</p></span>
        </div>
        <div class="article--details phoenix__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content/Cabaret/Mandzou 2014 04 Le Phoenix (6).jpg" alt="">                        
                    <img src="content/Cabaret/Mandzou 2014 04 Le Phoenix (7).jpg" alt="">
                    <img src="content/Cabaret/Mandzou 2014 04 Le Phoenix (18).jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content/Cabaret/Mandzou 2014 04 Le Phoenix (3).jpg" alt="">
        </div>
    </div>     

     
    <span id="retour"><p>Haut de la page</p></span>
    <?php include_once ("footer.php")?>
</div>
</body>
<script src="jquery-3.6.0.min.js"></script>
<script src="script.js"></script>