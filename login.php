<?php

include("variables.php");

if (isset($_POST["email"]) && isset($_POST["password"])){
    foreach ($users as $user) {
        if ($user["email"] === $_POST["email"] && $user["password"] === $_POST["password"])
          {
            $_SESSION['LOGGED_USER'] = $user["pseudo"];
        } else {
            $errorMessage = "email ou mot de passe invalide";}
    }
}
?>

<?php if(!isset($_SESSION["LOGGED_USER"])): ?>
<form action="index.php" method="post">
    <?php if (isset($errorMessage)) : ?>
    <div>
        <?php echo $errorMessage; ?>
    </div>        
    <?php endif; ?>
    <div>
        <label for="email">Email</label>
        <input type="email" name="email" id="email" aria-describedby="email-help" placeholder="Votre Email">
        <p id="email-help"> L'Email utilisé lors de la création du compte</p>
    </div>
    <div>
        <label for="password">Mot de passe</label>
        <input type="password" name="password" id="password">
    </div>
    <button type="submit">Connexion</button>
</form>

<?php else: ?>
    <div>
        <p>Bonjour <?php echo ($_SESSION["LOGGED_USER"]); ?> et bienvenue sur le site !</p>
    </div>
<?php endif; ?>