<!DOCTYPE html>
<html lang="fr">
<?php include_once ("head.php")?>
<body>
<?php include_once ("nav.php")?>
<div class="all"> 
    <div class="acceuil">
        <a href="index.php"><img src="content/logo/fleche-fine-contour-vers-la-gauche.png" alt=""><h2>accueil</h2></a>
    </div>   
    <div class="wedding">   
        <div class="article--banner feeBleu__bckgrnd--1">
            <span><p>La Fée Bleu</p></span>
        </div>
        <div class="article--details feeBleu__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content\Nymphe\LaFéeBleue9.jpg" alt="">
                    <img src="content\Nymphe\LaFéeBleue5.jpg" alt="">                        
                    <img src="content\Nymphe\LaFéeBleue14.jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content\Nymphe\LaFéeBleue22.jpg" alt="">
        </div>
    </div> 
    <div class="wedding">   
        <div class="article--banner  feuFollet__bckgrnd--1">
            <span><p>Denïtza le Feu Follet de L'Hiver</p></span>
        </div>
        <div class="article--details article__details--alt feuFollet__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content\Nymphe\2017 Denïtza le Feu Follet de l_Hiver (27).jpg" alt="">
                    <img src="content\Nymphe\2017 Denïtza le Feu Follet de l_Hiver (33).jpg" alt="">                        
                    <img src="content\Nymphe\2017 Denïtza le Feu Follet de l_Hiver (28).jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content\Nymphe\2017 Denïtza le Feu Follet de l_Hiver (6).jpg" alt="">
        </div>
    </div>  
    <div class="wedding">   
        <div class="article--banner Priyanka__bckgrnd--1">
            <span><p>Priyanka</p></span>
        </div>
        <div class="article--details Priyanka__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content\Nymphe\Priyanka_2.jpg" alt="">                        
                    <img src="content\Nymphe\Priyanka_3.jpg" alt="">
                    <img src="content\Nymphe\Priyanka_1.jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content\Nymphe\Priyanka_4.jpg" alt="">
        </div>
    </div>     
    <?php include_once ("CTA.php")?>
    </div> 
    <div class="wedding">   
        <div class="article--banner Numia__bckgrnd--1">
            <span><p>Nümia et le rayonnement de Litha</p></span>
        </div>
        <div class="article--details article__details--alt Numia__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content/Nymphe/2020 06 Nümia _ le rayonnement de Litha  (15).jpg" alt="">
                    <img src="content/Nymphe/2020 06 Nümia _ le rayonnement de Litha  (40).jpg" alt="">                        
                    <img src="content/Nymphe/2020 06 Nümia _ le rayonnement de Litha  (2).jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content/Nymphe/2020 06 Nümia _ le rayonnement de Litha  (44).jpg" alt="">
        </div>
    </div>     
    <div class="wedding">   
        <div class="article--banner Nymphe__bckgrnd--1">
            <span><p>Nymphe</p></span>
        </div>
        <div class="article--details Nymphe__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content/Nymphe/Mandzou 2013 Nymphe (1).jpg" alt="">
                    <img src="content/Nymphe/Mandzou 2013 Nymphe (7).jpg" alt="">                        
                    <img src="content/Nymphe/Mandzou 2013 Nymphe (9).jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content/Nymphe/Mandzou 2013 Nymphe (4).jpg" alt="">
        </div>
    </div>     
    <div class="wedding">   
        <div class="article--banner songe__bckgrnd--1">
            <span><p>Songe Givrée</p></span>
        </div>
        <div class="article--details article__details--alt songe__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content/Nymphe/Mandzou 2020 Songe Givrées (57).jpg" alt="">
                    <img src="content/Nymphe/Mandzou 2020 Songe Givrées (76).jpg" alt="">                        
                    <img src="content/Nymphe/Mandzou 2020 Songe Givrées (78).jpg" alt="">
                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content/Nymphe/Mandzou 2020 Songe Givrées (48).jpg" alt="">
        </div>
    </div>     
    <div class="wedding">   
        <div class="article--banner idunna__bckgrnd--1">
            <span><p>Idunna l'aura du printemps</p></span>
        </div>
        <div class="article--details idunna__bckgrnd--2">
            <div>  
                <div class="article--photo">              
                    <img src="content\Nymphe\Mandzou 2021 05 13 Idunna l_aura du printemps (72).jpg" alt="">
                    <img src="content\Nymphe\Mandzou 2021 05 13 Idunna l_aura du printemps (42).jpg" alt="">                        
                    <img src="content\Nymphe\Mandzou 2021 05 13 Idunna l_aura du printemps (13).jpg" alt="">
                    <img src="content\Nymphe\Mandzou 2021 05 13 Idunna l_aura du printemps (36).jpg" alt="">

                </div>
                <div class="article--text">
                    <p>
                        Vous êtes sur le point de vous marier et vous êtes dans les préparatifs. <br> 
                        Eglise, Mairie, Salle et tous ces petits détails qui prennent un temps considérable… <br> 
                        Vient le détail de l’accessoire de coiffure <br>
                        « que mettre ? » <br> 
                        « Comment se coiffer ? »<br>
                        Pas de soucis, voici tous les détails dont j’ai besoin pour vous conseiller au mieux dans le choix de l’accessoire de coiffure ! 
                    </p>
                    <p>
                        A savoir : je conçois la parure à la date de votre essai coiffure, afin que votre coiffeuse puisse prendre en main, l’accessoire avant le jour J. <br>
                        Je réalise également les boutonnières assorties à votre accessoire de coiffure pour votre futur époux. (Modèle sur broche ou à épingler). <br>
                        Je réalise aussi des accessoires de coiffure pour votre (vos) témoin(s), dans le même esprit que votre accessoire de coiffure et/ou votre thème.
                    </p>
                </div>
            </div>
            <img src="content\Nymphe\Mandzou 2021 05 13 Idunna l_aura du printemps (28).jpg" alt="">
        </div>
    </div>      
    <span id="retour"><p>Haut de la page</p></span>
    <footer>
    <?php include_once ("footer.php")?>
    </footer>
</div>
</body>
<script src="jquery-3.6.0.min.js"></script>
<script src="script.js"></script>