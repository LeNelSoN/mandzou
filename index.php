<!DOCTYPE html>
<html lang="fr">
<?php include_once ("head.php")?>
<body>
<?php include_once ("nav.php")?>
    <div class="all">
        <div class="social">                     
            <ul class="social">           
                <li><a href="https://www.facebook.com/mandzou" target="_blank" rel="noopener noreferrer"><img src="content/logo/facebook.png" alt="Lien Facebook" class="facebook" title="Follow on Facebook"></a>
                </li>
                <li><a href="https://www.instagram.com/mandzou/" target="_blank" rel="noopener noreferrer"><img src="content/logo/instagram.png" alt="Lien Instagram" class="instagram" title="Follow on Instagram"></a> 
                </li>
                <li><a href="http://www.etsy.com/fr/shop/Mandzou" target="_blank" rel="noopener noreferrer"><img src="content/logo/etsy.png" alt="Lien Etsy" class="etsy" title="Follow on Etsy"></a>
                </li>
            </ul>
        </div>
        <div>
            <div id="diaporama"></div>
<?php include_once ("CTA.php")?>
            <div class="categories">   
                <div class="details">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam tempora, eligendi optio, provident laudantium vel accusamus quae ut facere sit odit esse fugiat obcaecati vitae ex atque. Laudantium, explicabo tempore?</p>
                    <div class="img1">
                        <span>
                            <h2>Accessoires</h2>
                        </span>
                    </div>
                </div>
                <div class="details details--alt">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam tempora, eligendi optio, provident laudantium vel accusamus quae ut facere sit odit esse fugiat obcaecati vitae ex atque. Laudantium, explicabo tempore?</p>
                    <div class="img1">
                        <span>
                            <h2><a href="mariage.html"><h2 id = weding > Mariage </h2></a></h2>
                        </span>
                    </div>
                </div>
                <div class="details">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam tempora, eligendi optio, provident laudantium vel accusamus quae ut facere sit odit esse fugiat obcaecati vitae ex atque. Laudantium, explicabo tempore?</p>
                    <div class="img1">
                        <span>
                            <h2>Costume</h2>
                        </span>
                    </div>
                </div>
            </div> 
            <span id="retour"><p>Haut de la page</p></span>
        </div>        
<?php include_once ("footer.php")?>
    </div>
</body>
<script src="jquery-3.6.0.min.js"></script>
<script src="script.js"></script>
</html>