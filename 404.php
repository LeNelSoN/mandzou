<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mandzou</title>
    <link rel="shortcut icon" href="content/logo/favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="ccs/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cinzel+Decorative&family=Cinzel:wght@400;500;600&display=swap" rel="stylesheet"> 
</head>
<body>
<?php include_once("nav.php"); ?>
        <div class="acceuil">
            <a href="index.php"><img src="content/logo/fleche-fine-contour-vers-la-gauche.png" alt=""><h2>accueil</h2></a>
        </div>
        <div class="_404">
            <h2>En Construction</h2>
            <img src="content\Photo\Diapo 1.jpg" alt="">
        </div>
<?php include_once("footer.php"); ?>
</body>
<script src="jquery-3.6.0.min.js"></script>
<script src="script.js"></script>
</html>